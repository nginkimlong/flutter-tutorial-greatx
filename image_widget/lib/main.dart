import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyImage(title: 'Welcome back' ),
    );
  }
}

class MyImage extends StatelessWidget {
  @override
  MyImage({Key key, this.title}): super(key: key);
  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(this.title)),
      body: Center(
        // child: Icon(
        //   Icons.description
        //
        // ),

        child: Image.asset('assets/greatX.jpeg'),
      ),
    );
  }
}
