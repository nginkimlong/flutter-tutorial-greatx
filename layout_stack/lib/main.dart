import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

Widget listCard() => Stack(
  alignment: const Alignment(0.6, 0.6),
  children: [
    CircleAvatar(
      backgroundImage: AssetImage('assets/1.jpeg'),
      backgroundColor: Colors.black12,
    ),
    Container(
      decoration: BoxDecoration(
        color: Colors.black54
      ),
      child: Text(
        'Overlaid subsequence child',
        style: TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.bold,
          color: Colors.white
        ),
      ),
    ),
  ],
);

class MyHomePage extends StatelessWidget {
  final String title;
  MyHomePage({Key key, this.title}):super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Flutter App Bar')),
      body: listCard(),
    );
  }
}
