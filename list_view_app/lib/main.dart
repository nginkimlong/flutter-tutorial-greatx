
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'List View App',
      theme: ThemeData(
        // This is the theme of your application.
        primarySwatch: Colors.blue,
      ),
      home: MyListView(title : '123'),
    );
  }
}

class MyListView extends StatelessWidget {
  final String title;
  final List<String> entries = ['A', 'B', 'C'];
  final List<int> colorCode = [600, 500, 800];

  @override
  MyListView({Key key, this.title}) :super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(this.title),
        leading: GestureDetector(
          onTap: () { /* Write listener code here */ },
          child: Icon(
            Icons.menu,  // add custom icons also
          ),
        ),
        actions: <Widget>[
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {},
                child: Icon(
                  Icons.search,
                  size: 26.0,
                ),
              )
          ),
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {},
                child: Icon(
                    Icons.more_vert
                ),
              )
          ),
        ],
      ),
      // body:
      //     ListView(
      //       padding: const EdgeInsets.all(8),
      //       children: <Widget>[
      //         Container(
      //           height: 50,
      //           color: Colors.amber[600],
      //           child: const Center(child: Text('Entry A')),
      //         ),
      //         Container(
      //           height: 50,
      //           color: Colors.amber[500],
      //           child: const Center(child: Text('Entry B')),
      //         ),
      //         Container(
      //           height: 50,
      //           color: Colors.amber[100],
      //           child: const Center(child: Text('Entry C')),
      //         ),
      //       ],
      //
      // )
      body: ListView.separated(
        padding: const EdgeInsets.all(8.0),
        itemCount: entries.length,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              height: 50,
              color: Colors.amber[colorCode[index]],
              child: Center(child: Text('Entry ${entries[index]}')),
            );
          },
        separatorBuilder: (BuildContext context, int index) => const Divider(),
      ),
    );
  }
}


