import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: OurHome(),
    );
  }
}

Widget _buildDecoratedImage(int imageIndex) {
  return Expanded(
    child: Container(
      decoration: BoxDecoration(
        border: Border.all(width: 10, color: Colors.black38),
        borderRadius: const BorderRadius.all(const Radius.circular(8)),
        color: Colors.black12,
      ),
      margin: const EdgeInsets.all(4),
      child: Image.asset('assets/$imageIndex.jpeg'),
    ),
  );
}

Widget _buildImageRow(int imageIndex) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        _buildDecoratedImage(imageIndex),
        _buildDecoratedImage(imageIndex+1),
      ],
    );
}


class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        mainAxisSize: MainAxisSize.min,
        children: [
          _buildImageRow(3),
          _buildImageRow(2),
        ],
      ),
    );
  }
}

class OurHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Container Layout')),
      body: MyHomePage(),
    );
  }
}



