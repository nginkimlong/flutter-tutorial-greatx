import 'package:flutter/material.dart';
import 'listDetail.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}
Widget _buildGrid() => GridView.extent(
    maxCrossAxisExtent: 120,
    padding: const EdgeInsets.all(3),
    mainAxisSpacing: 4,
    crossAxisSpacing: 4,
    // children: _displayingGridItem(8),
    children: _getTitle(8),
);

void _onTileClicked(int index){
  // debugPrint("You tapped on item $index");
  runApp(ListDetail(index: index));
}

List<Widget> _getTitle(int count)
{
  return List.generate(count, (index) => GridTile(
    child: new InkResponse(
      child: Image.asset('assets/$index.jpeg'),
      onTap: () => _onTileClicked(index),
    ),
  )
  );
}

class MyHomePage extends StatelessWidget {
  final String title;
  MyHomePage({Key key, this.title}):super(key: key);
  @override

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Grid View')),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: _buildGrid(),
      ),
    );
  }
}


// The images are saved with names pic0.jpg, pic1.jpg...pic29.jpg.
// The List.generate() constructor allows an easy way to create
// a list when objects have a predictable naming pattern.
List<Container> _displayingGridItem(int count)
{
//   Generates a list of values.
//   Creates a list with length positions and fills it with values created by calling generator for each index in the range 0 .. length - 1 in increasing order.
// List .generate(3, (int index) => index * index); // [0, 1, 4]
// The created list is fixed-length if growable is set to false.
// The length must be non-negative.
  return List.generate(count, (index) => Container(
        child:  Image.asset('assets/$index.jpeg'),
      )
  );
}


