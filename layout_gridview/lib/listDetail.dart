import 'package:flutter/material.dart';
import 'main.dart';

void main()
{
  runApp(ListDetail());
}

class ListDetail extends StatelessWidget {
  final int index;
  ListDetail({Key key, this.index}): super(key: key);

  void showDisplay(int id)
  {
    print('You have clicked: $id');
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: SubListDetail(title: 'Detail of App ${index}'),
    );
  }
}
class SubListDetail extends StatelessWidget {
  final String title;
  SubListDetail({Key key, this.title}):super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => runApp(MyApp()),
        ),
        title: Text("Sample"),
        centerTitle: true,
      ),
      body: Container(
        padding: EdgeInsets.all(8.0),
        child: Text('Home page detail',
        style: TextStyle(
          fontSize: 14,
          fontWeight: FontWeight.bold,
        ),
          textAlign: TextAlign.center,
        ),
      ),

    );
  }
}






