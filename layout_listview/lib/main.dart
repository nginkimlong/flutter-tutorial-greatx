import 'dart:developer';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter simple App',
      theme: ThemeData(backgroundColor: Colors.black12),
      home: CounterNumber(title: "Fluter simple App bar"),
    );
  }
}

class CounterNumber extends StatefulWidget {
  final String title;
  CounterNumber({Key key, this.title}):super(key: key);

  @override
  _CounterNumberState createState() => _CounterNumberState();
}

class _CounterNumberState extends State<CounterNumber> {
  int _count=0;
  void _counterAddNumber()
  {
    setState(() {
      _count++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Counter Number')),
      body: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text("You click: ${_count}"),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _counterAddNumber,
        child: Icon(Icons.add),
      ),
    );
  }
}
