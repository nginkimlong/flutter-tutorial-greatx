import 'package:intl/intl.dart';
import 'package:mvvm_news/model/modelfile.dart';

class ViewModel{
  ModelNewsArticle _modelNewsArticle;
  ViewModel({ModelNewsArticle article}): _modelNewsArticle = article;

  String get title{
    return _modelNewsArticle.title;
  }
  String get description {
    return _modelNewsArticle.description;
  }
  String get urlToImage
  {
    return _modelNewsArticle.urlToImage;
  }
  String get url
  {
    return _modelNewsArticle.url;
  }
  String get publishedAt
  {
    final dateTime = DateFormat('yyyy-mm-dd').parse(_modelNewsArticle.publishedAt, true);
    return DateFormat.yMMMMEEEEd('en-us').format(dateTime);
  }

}