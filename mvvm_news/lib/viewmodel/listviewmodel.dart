

import 'package:flutter/cupertino.dart';
import 'package:mvvm_news/model/modelfile.dart';
import 'package:mvvm_news/service/service.dart';
import 'package:mvvm_news/viewmodel/viewmodel.dart';


//This statement for filter conditions
enum LoadingStatus
{
  Completed,
  Searching,
  Empty,
}

class ListViewModel with ChangeNotifier
{
  LoadingStatus loadingStatus = LoadingStatus.Empty;
  List<ViewModel> articles = List<ViewModel>();

  void fetchTopNewsHeadlines() async
  {
    List<ModelNewsArticle> _newsarticle = await WebService().getTopHeadLines();
    loadingStatus = LoadingStatus.Searching;
    notifyListeners();
    this.articles = _newsarticle.map((article) => ViewModel(article: article)).toList();
    if(this.articles.isEmpty)
    {
      loadingStatus = LoadingStatus.Empty;
    }else{
      loadingStatus = LoadingStatus.Completed;
    }
    // notifyListeners();
  }

  // methods for news from each countries
  void fetchNewsByCountry(String country) async
  {
    List<ModelNewsArticle> _newsarticle = await WebService().getNewsByCountry(country);
    loadingStatus = LoadingStatus.Searching;
    notifyListeners();
    this.articles = _newsarticle.map((article) => ViewModel(article: article)).toList();
    if(this.articles.isEmpty)
    {
      loadingStatus = LoadingStatus.Empty;
    }else{
      loadingStatus = LoadingStatus.Completed;
    }

    notifyListeners();
  }



}