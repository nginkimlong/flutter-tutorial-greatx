class Constants
{
  static const APIKEY = "a08d77da7eed461cb351e731142e6bdf";
  static const TOPHEADLINES = "https://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=$APIKEY";

  static String headlineFor(String country)
  {
    return "https://newsapi.org/v2/top-headlines?country=$country&category=business&apiKey=$APIKEY";
  }

  static const Map<String, String> countries = {
    'USA': 'us',
    'China': 'cn',
    'Khmer': 'km'

  };
}