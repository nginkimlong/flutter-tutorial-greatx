import 'package:dio/dio.dart';
import 'package:mvvm_news/constant/constant.dart';
import 'package:mvvm_news/model/modelfile.dart';

class WebService
{
  var dio = Dio();

  // for top headlines
  Future<List<ModelNewsArticle>> getTopHeadLines() async
  {
    final response = await dio.get(Constants.TOPHEADLINES);

    if (response.statusCode == 200) // 200 represent successful response
    {
      final result = response.data;
      Iterable list = result['articles'];
      return list.map((article) => ModelNewsArticle.fromJson(article)).toList();
    }else{
      throw Exception('Response failed');
    }
  }

  // for top headlines
  Future<List<ModelNewsArticle>> getNewsByCountry(String country) async
  {
    final response = await dio.get(Constants.headlineFor(country));

    if (response.statusCode == 200) // 200 represent successful response
    {
      final result = response.data;
      Iterable list = result['articles'];
      return list.map((article) => ModelNewsArticle.fromJson(article)).toList();
    }else{
      throw Exception('Response failed');
    }
  }



}
