import 'package:flutter/material.dart';
import 'package:mvvm_news/screen/mainscreen.dart';
import 'package:mvvm_news/viewmodel/listviewmodel.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      //Initial Route
      home: MultiProvider(
        providers: [
          ChangeNotifierProvider(
              create: (_) => ListViewModel(),
          )
        ],
        child: NewsScreen(),
        // child: Text("TESTING MULTIPLE PROVIDER"),
      ),
    );
  }
}

