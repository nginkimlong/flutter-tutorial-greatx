
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Changing App",
      theme: ThemeData(backgroundColor: Colors.white),
      home: ChangingApp(title: 'Changing App Bar'),
    );
  }
}

class ChangingApp extends StatelessWidget {
  final String title;
  ChangingApp({Key key, this.title}):super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(this.title)),
      body: Counter(),
    );
  }
}

class Counter extends StatefulWidget {
  @override
  _CounterState createState() => _CounterState();
}

class _CounterState extends State<Counter> {
  int counter = 0;
  void increasingCounter()
  {
    setState(() {
      counter++;
    });
  }
  void descreasingCounter()
  {
    setState(() {
      counter--;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Row(

      children: <Widget>[
        ElevatedButton(
            onPressed: increasingCounter,
            child: Text("Count me")
        ),
        SizedBox(width: 6),
        ElevatedButton(
            onPressed: descreasingCounter,
            child: Text("Discreasing")
        ),
        SizedBox(width: 6),
        Text('Count: $counter'),

      ],
    );
  }
}
