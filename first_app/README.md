# first_app

A new Flutter application.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)
============================================================

The use Flutter sample:

1. The use StatelessWidget វាមានតួនាទីសំខាន់ក្នុងការក្តោបនូវ Widget ផ្សេងទៀត

A widget that does not require mutable state. A stateless widget is a widget that describes part of
the user interface by building a constellation of other widgets that describe the user interface more concretely.
The building process continues recursively until the description of the user interface is fully concrete
(e.g., consists entirely of RenderObjectWidgets, which describe concrete RenderObjects).
www.youtube.com/watch?v=wE7khGHVkYY
Stateless widget are useful when the part of the user interface you are
describing does not depend on anything other than the configuration information in the object itself
and the BuildContext in which the widget is inflated. For compositions that can change dynamically,
e.g. due to having an internal clock-driven state, or depending on some system state, consider using StatefulWidget.




- វាអនុញ្ញាតអោយយើងធ្វើការ ហៅនូវ stateless tree ជាច្រើនដើម្បីមកប្រើប្រាស់បាន។
- StatelessWidget យើងគួរបង្កើតអោយបានច្រើននូវ const (constructor នៃ class) រួចហៅវាទៅប្រើប្រាស់នៅក្នុង stateless
- ChickenApp មានតួនាទីបង្ហាញប្រតិបត្តិការសម្រាប់ App ​ទាំងមូលនោះត្រូវបានហៅថា stateless
- stateless cannot track data overtime or trigger rebuild own their own, to do so we have to use statefull widget

2. Understanding some about our sample app:
- the starting point is in void main() { ChickenApp() }

