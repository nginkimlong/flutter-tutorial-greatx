import 'package:flutter/material.dart';

void main() {
  runApp(ChickenApp()); // starting point

}
class ChickenApp extends StatelessWidget
{
  Widget build(BuildContext context)
  {
    return MaterialApp(
      title: "GreateX Institute",
      home: Scaffold(
        appBar: AppBar(
          title: Text("Welcome to GreatX institute"),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ChickenName("Black Chicken"),
              ChickenName("White Chicken"),
              ChickenName("Super Chickent"),
            ],
          ),

        ),
      ),
    );
  }
}


class ChickenName extends StatelessWidget
{
  final String name;
  const ChickenName(this.name);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return  DecoratedBox(
          decoration: BoxDecoration(color: Colors.lightBlueAccent),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(name),
          ),
        );
  }
}